-- UCI Controller Layer Framework
-- James Martin, QSC UK 2021
-- v1.0

-- provides a way to create repeatable UCIs with all the navigation logic included
-- logic can be changed programmatically based on the NavGroups and Popups table
-- no layer controller required
-- designed for '1 page, lots of layers' rather than >1 page UCIs

-- Set these as needed
PageName = 'Page 1'
NavTransition = 'none'
PopUpTransition = 'none'

NavGroups = {
  -- each item in here represents a layer or group of layers representing a navigation page or subgroup
  -- give it a Name, which navigation control to use, and which layers are shown
  -- groups are mutually exclusive, unlike popups
	{
		Name = 'Phone',
		Control = Controls.Nav_1,
		Layers = {'Phone',},
	} ,
  {
		Name = 'Sound',
		Control = Controls.Nav_2,
		Layers = {'Sound'},
	} ,
  {
		Name = 'Camera',
		Control = Controls.Nav_3,
		Layers = {'Camera'},
	} ,
 {
		Name = 'Video',
		Control = Controls.Nav_4,
		Layers = {'Video'},
	} ,
}

Popups = {
  -- ShowOnGroups is optional list group indexes that popup is allowed on
  -- if not present it will show on any group
  -- if ShowOnGroups is present for the popup, it will only show if that group is active
  -- if Transition is present for the popup, it will be used, otherwise global PopUpTransition will be used
  
  {
    Control = Controls.InCall,
    Layer = 'In Call',
    Transition = 'top'
  },
  {
    Control = Controls.AutoPTZ,
    Layer = 'AutoCamera',
    ShowOnGroups = {3}
  },
 {
    Control = Controls.Settings,
    Layer = 'PWPrompt',
    Transition = 'none'
  },
}




--CODE BELOW:
---------------------------------------------------------------------------------------------------------
-- Set's layers and prints what happened
function SetLayer(layer, state, transition)
  print('\tSet Layer:'	,	layer,	state)
	Uci.SetLayerVisibility(PageName, layer, state, transition)
end

function ValueIn(value,tbl)
  for i,v in pairs(tbl) do
    if v == value then
      return(true)
    end
  end
  return false
end

CurrentNavGroup = nil



function NavGroupChange(groupIndex)
-- takes a group number and set's the layers and control to true
-- turns all other group layers and controls false
-- Calls SetPopups
	print('NavGroupChange'	,	groupIndex, NavGroups[groupIndex].Name)

  for i,group in pairs(NavGroups) do
    local groupState = groupIndex == i
    if groupState then CurrentNavGroup = groupIndex end
    group.Control.Boolean = groupState

		-- Set Layers
    for _,layer in pairs(group.Layers) do
			SetLayer(layer, groupState, NavTransition)
    end
  end

  for i,Popup in pairs(Popups) do
	  SetPopups(i)
  end

end

function SetPopups(PopupIndex)
-- sets popup layer state based on corresponding control

  local Popup = Popups[PopupIndex]
	local layer = Popup.Layer
	print('SetPopups'	,	PopupIndex	, layer)
  local state = Popup.Control.Boolean

  if Popup.ShowOnGroups then
    state = ValueIn(CurrentNavGroup , Popup.ShowOnGroups) and Popup.Control.Boolean
    print('\tShow for this group:',state)
  end
  local transition = Popup.Transition or PopUpTransition

	print('\tstate:',state)
	SetLayer(layer, state, transition)
end


-- set eventhandlers and runs functions on startup
for i,group in pairs(NavGroups) do
	--EventHandler for main control
  group.Control.EventHandler = function()
    NavGroupChange(i)
  end
  NavGroupChange(i)
end

for i,Popup in pairs(Popups) do
	Popup.Control.EventHandler = function()
		SetPopups(i)
	end
	SetPopups(i)
end