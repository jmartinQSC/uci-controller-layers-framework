This script will let you programmatically set the behavior of layers in your UCI using the UCI Script feature in 9.2.

 

I like using the UCI layer controller and logic blocks to drive UCI navigation, but this can become tricky to manage on complex UCIs, difficult to edit without recompiling the design, and needs to be copied and reassigned if you copy the UCI.

On the other hand, using scripting can be overly complex for simple UCIs, and can be less intuitive to work with, especially if it isn't your code.

 

In this script, you just need to list which layers are in which navigation 'group', and what control controls that group. This is done in a lua table:

NavGroups = {
  -- each item in here represents a layer or group of layers representing a navigation page or subgroup
  -- give it a Name, which navigation control to use, and which layers are shown
  -- groups are mutually exclusive, unlike popups
	{
		Name = 'Phone',
		Control = Controls.Nav_1,
		Layers = {'Phone',},
	} ,
  {
		Name = 'Sound',
		Control = Controls.Nav_2,
		Layers = {'Sound'},
	} ,
}
There is also a table for Popup layers, which can popup anywhere, or only on certain Navigation Groups:

 

Popups = {
  -- ShowOnGroups is optional list group indexes that popup is allowed on
  -- if not present it will show on any group
  -- if ShowOnGroups is present for the popup, it will only show if that group is active
  -- if Transition is present for the popup, it will be used, otherwise global PopUpTransition will be used
  
  {
    Control = Controls.InCall,
    Layer = 'In Call',
    Transition = 'top'
  },
  {
    Control = Controls.AutoPTZ,
    Layer = 'AutoCamera',
    ShowOnGroups = {3}
  },
}
All you need to do is create the controls that you want to drive the navigation and popups from the UCI Toolbox, and create the layers, and then add them to these tables. The script will then take care of the transitions, mutually exclusive buttons, and can be expanded with additional EventHandlers as needed.

 

Lastly, if you copy the UCI, all the logic comes with it as part of the script.